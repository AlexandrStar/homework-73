const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;

const app = express();

const port = 8001;

const pass = 'mamastiflera';

app.get('/encode/:code', (req, res) => {
  const encode = Vigenere.Cipher(pass).crypt(req.params.code);
  res.send(encode);
});

app.get('/decode/:code', (req, res) => {
  const decode = Vigenere.Decipher(pass).crypt(req.params.code);
  res.send(decode)
});

app.listen(port, () => {
  console.log('We are live on ' + port);
});





